#!/usr/bin/python
import time
import datetime

now = time.time()

html = """
<html>
    <body>
        <p>Aktualny czas: %s</p>
        <p>a czytelniej: %s</p>
        <p>oraz w formacie ISO: %s</p>
    </body>
</html>
""" % (now,
       datetime.datetime.fromtimestamp(now),
       datetime.datetime.fromtimestamp(now).isoformat())

print "Content-Type: text/html"
print "Content-length: %s" % len(html)
print
print html