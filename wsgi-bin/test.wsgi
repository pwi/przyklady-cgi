#!/usr/bin/python
# -*- coding: utf-8 -*-

# Główny obiekt aplikacji.
# Używając mod_wsgi trzeba go nazwać "application"
def application(environ, start_response):

    # Budowa ciała odpowiedzi
    response_body = 'Metoda żądania to %s' % environ['REQUEST_METHOD']

    # Status HTTP
    status = '200 OK'

    # Nagłówki HTTP oczekiwane przez klienta.
    # Powinny być podane w postaci listy zawierającej dwuelementowe krotki
    # [(Nazwa nagłówka, Wartość nagłówka)].
    response_headers = [('Content-Type', 'text/plain; charset=utf-8'),
                        ('Content-Length', str(len(response_body)))]

    # Wysłanie ich do serwera przy użyciu dostarczonej metody (callback)
    start_response(status, response_headers)

    # Zwrot ciała odpowiedzi
    # Opakowane w listę, ale może to być dowolny obiekt typu iterable
    return [response_body]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('localhost', 4444, application)
    srv.serve_forever()